function savedb(data) {
  return new Promise(function(resolve, reject) {
         setTimeout(run_save, 1000, data, function(res){
               if(res){
                  resolve(res)
               } else {
                  reject(res)
               }
         });
     }
  );
}

function run_save(data, callback) {
       if (data.toString().length < 5) {
          callback(false);
       }else {
          callback(true)
       }
}

class Users {
  constructor(name) {
    this.name = name;
  }
  save(callback) {
    savedb(this.name).then(
                        ()=> callback()
                      ).catch(
                         (res)=> callback(res));
  }
}

module.exports = Users;
