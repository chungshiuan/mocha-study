var sinon = require('sinon');
var rewire = require('rewire');
var expect = require('chai').expect;
var run_callback = require('./yourfunc').run_callback;
var run_set_name = require('./yourfunc').run_set_name;
var run_no_export_func_to_set_name =  require('./yourfunc').run_no_export_func_to_set_name;
var run_no_export_obj_to_set_name =  require('./yourfunc').run_no_export_obj_to_set_name;
var user = require('./yourfunc').user;
var yf = rewire('./yourfunc');

describe('sion examples', function(){
   beforeEach(function(){
      yf = rewire('./yourfunc');
      //console.log(yf);

   });
   describe('sion spy examples', function(){
     it('an anonymous function example', function(){
         var callback = sinon.spy(); //use spy to create a callback
         run_callback(10, callback);
         expect(callback.called).to.be.ok;
         expect(callback.calledWith(10)).to.be.ok;
     });

     it('wrap an existing method',function(){
         var setNameSpy = sinon.spy(user, 'setName'); //use spy to wrap setName
         run_set_name('Rick')
         expect(setNameSpy.callCount).to.be.equal(1);
         setNameSpy.restore();
     })

     it('wrap an no exported obj method',function(){
         var setNameSpy = sinon.spy(yf.__get__('no_export_user'), 'setName' )
//         yf.__set__('no_export_user', {setName: setNameSpy})
         yf.run_no_export_obj_to_set_name('Rick')
         expect(setNameSpy.callCount).to.be.equal(1);
         setNameSpy.restore();
     })

     it('wrap an no exported func method',function(){
         var setNameSpyFunc = sinon.spy(yf.__get__('func_set_name'));
         yf.__set__('func_set_name', setNameSpyFunc)
         yf.run_no_export_func_to_set_name('Rick');
         expect(setNameSpyFunc.callCount).to.be.equal(1);
         //setNameSpyFunc.restore();
     })
   });
   describe('sion stub examples', function(){
     it('wrap an no exported obj method',function(){
         var setNameStub = sinon.stub(yf.__get__('no_export_user'), 'setName' )
         setNameStub.withArgs('rick').returns('RICK')
//         yf.__set__('no_export_user', {setName: setNameStub})
         var result = yf.run_no_export_obj_to_set_name('rick')
         expect(result).to.be.equal('RICK');
         setNameStub.restore();
     })
     it('wrap an no exported obj method',function(){
         var setNameStub = sinon.stub(yf.__get__('no_export_user'), 'setName' )
         //setNameStub.withArgs('rick').returns('RICK')
         setNameStub.callsFake( function(x) {
                                    console.log('123');
                                   return 'RICK';
                               })
//         yf.__set__('no_export_user', {setName: setNameStub})
         var result = yf.run_no_export_obj_to_set_name('rick')
         expect(result).to.be.equal('RICK');
         setNameStub.restore();
     })
     it('wrap an no exported obj method by func2',function(){
         var yf2 = rewire('./yourfunc2');
         var setNameStub = sinon.stub(yf.__get__('no_export_user'), 'setName' )
         //setNameStub.withArgs('rick').returns('RICK')
         setNameStub.callsFake( function(x) {
                                    console.log('123');
                                   return 'RICK';
                               })
         yf2.__set__('callByFunc2', yf.__get__('callByFunc2'))
//       var run_no_export_obj_to_set_name2 = require('./yourfunc2').run_no_export_obj_to_set_name2
         var result = yf2.run_no_export_obj_to_set_name2('rick')
         expect(result).to.be.equal('RICK');
         setNameStub.restore();
     })


     it('wrap an no exported func method',function(){
         var setNameStub = sinon.stub(yf,'func_set_name');
         setNameStub.withArgs('rick').returns('RICK')
         yf.__set__('func_set_name', setNameStub)
         var result = yf.run_no_export_func_to_set_name('rick');
         expect(result).to.be.equal('RICK');
         //setNameStub.restore();
     })

     it('wrap an no exported func method',function(){
         var setNameStub = sinon.stub();
         setNameStub.withArgs('rick').returns('RICK')
         yf.__set__('func_set_name', setNameStub)
         var result = yf.run_no_export_func_to_set_name('rick');
         expect(result).to.be.equal('RICK');
         //setNameStub.restore();
     })
   });
   describe('sion mock examples', function(){
     it('wrap an no exported obj method',function(){
         var setNameMock = sinon.mock(yf.__get__('no_export_user'))
         var func = setNameMock.expects('setName').withArgs('rick').returns('RICK')
//         yf.__set__('no_export_user', {setName: func})
         var result = yf.run_no_export_obj_to_set_name('rick')
         expect(result).to.be.equal('RICK');
         setNameMock.verify()
     })
   });
   describe('sion mock examples', function(){
     it('wrap an no exported obj method',function(){
         var setNameMock = sinon.mock(yf.__get__('no_export_user'))
         setNameMock.expects('setName').withArgs('rick').returns('RICK')
//         yf.__set__('no_export_user', {setName: func})
         var result = yf.run_no_export_obj_to_set_name('rick')
         expect(result).to.be.equal('RICK');
         setNameMock.verify()
     })
   });

})
