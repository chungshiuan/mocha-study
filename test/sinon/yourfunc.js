function run_callback(x, callback) {
     callback(x);
}

var user = {
  setName: function(name){
    this.name = name;
  }
}
function func_set_name(name) {
   console.log('[yourfunc]func_set_name', name)
   return name; 
}

var no_export_user = {
  setName: function(name,x){
    this.name = name;
    console.log('[yourfunc] setName', name)
    return name;
  }
}

function run_set_name(name){
    var u = user.setName(name);
}
function run_no_export_obj_to_set_name(name){
    var u = no_export_user.setName(name);
    return u;}

function run_no_export_func_to_set_name(name){
    var u = func_set_name(name);
    return u;
}

function callByFunc2(name) {
    var u = no_export_user.setName(name);
    return u;
}
module.exports = {run_callback, 
                  user, 
                  run_set_name, 
                  run_no_export_obj_to_set_name,
                  run_no_export_func_to_set_name,
                  callByFunc2}
