var expect = require('chai').expect;

describe("a test", function(){
  var foo = false;

  beforeEach(function(){

    // simulate async call w/ setTimeout
    setTimeout(function(){
      foo = true;
    }, 50);

  });

  it("should pass", function(){
    setTimeout( function(){
       expect(foo).equals(true);
    }, 52);
  });

});
