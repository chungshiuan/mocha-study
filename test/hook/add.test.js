var add = require('./add.js');
var expect = require('chai').expect;

describe('****************' ,function() {
before(function(){
     console.log(' ******* before')
})
after( function(){
     console.log(' ******* after')
})

beforeEach(function(){
     console.log(' ******* beforeEach')
})
afterEach( function(){
     console.log(' ******* afterEach')
})

describe('-------------------' ,function() {
  before(function(){
     console.log(' ------- before')
  })
  after( function(){
     console.log(' ------- after')
  })
  beforeEach(function(){
     console.log(' ------- beforeEach')
  })
  afterEach( function(){
     console.log(' ------- afterEach')
  })

  it( '1 + 1 should be 2', function() {
    expect(add(1, 1)).to.be.equal(2);
  });
  it( 'x + 0 should be x', function(){
    expect(add(1,0)).to.be.equal(1);
  });
});


describe('====================' ,function() {
  before(function(){
     console.log(' ======= before')
  })
  after( function(){
     console.log(' ======= after')
  })
  beforeEach(function(){
     console.log(' ======= beforeEach')
  })
  afterEach( function(){
     console.log(' ======= afterEach')
  })

  it( '1 + 1 should be 2', function() {
    expect(add(1, 1)).to.be.equal(2);
  });
  it( 'x + 0 should be x', function(){
    expect(add(1,0)).to.be.equal(1);
  });
});
})
