var add = require('./add.js');
var expect = require('chai').expect;

describe('testing of an add method' ,function() {
  it( '1 + 1 should be 2', function() {
    expect(add(1, 1)).to.be.equal(2);
  });
  it( 'x + 0 should be x', function(){
    expect(add(1,0)).to.be.equal(1);
  });
});
