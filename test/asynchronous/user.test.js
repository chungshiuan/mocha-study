const User = require('./user.js');
describe('two ways to test User save', function() {
  
  describe('#complicated way to test save()', function() {
    it('should save without error', function(done) {
      var user = new User('LunaLuna');
      user.save(function(err) {
        if (!err) done(err);
        else done();
      });
    });
  });

  describe('easy way to test save()', function() {
    it('should save without error', function(done) {
      var user = new User('LunaLuna');
      user.save(done);
    });
  });

});
